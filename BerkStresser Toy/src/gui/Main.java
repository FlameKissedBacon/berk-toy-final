package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import bp.Toy;
import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import java.awt.Component;

import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import javax.swing.JCheckBox;

public class Main {

	private JFrame frmToyCircuitData;
	private JTextField txtbxInspector;
	private JTextField txtbxDate;
	private JTextField txtC1Res;
	private JTextField txtC2Volt;
	private JTextField txtC2Res;
	private JTextField txtC1Volt;
	private JComboBox<String> cboC1ManufactureLocation;
	private JComboBox<String> cboC2ManufactureLocation;
	private JButton btnSave;
	private JButton btnClearValues;

	private JCheckBox chckQuickEnter;
	private JCheckBox chckLock1;
	private JCheckBox chckLock2;
	private JButton btnDelete;
	private Toy myToy = new Toy();

	private static String[] COUNTRIES = { "", "China", "Germany", "United States" };
	private JTextField txtToyID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmToyCircuitData.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmToyCircuitData = new JFrame();
		frmToyCircuitData.setTitle("Toy Circuit Data Collector");
		frmToyCircuitData.setBounds(100, 100, 552, 446);
		frmToyCircuitData.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmToyCircuitData.getContentPane().setLayout(null);

		JLabel lblInspector = new JLabel("Inspector:");
		lblInspector.setBounds(26, 149, 95, 14);
		frmToyCircuitData.getContentPane().add(lblInspector);

		JLabel lblDate = new JLabel("Date:");
		lblDate.setBounds(51, 192, 70, 14);
		frmToyCircuitData.getContentPane().add(lblDate);

		txtbxInspector = new JTextField();
		txtbxInspector.setToolTipText("Enter your name or ID");
		txtbxInspector.addActionListener(new BtnSaveActionListener());
		txtbxInspector.addKeyListener(new ClearValueKeyListener());
		txtbxInspector.setBounds(139, 151, 335, 19);
		frmToyCircuitData.getContentPane().add(txtbxInspector);
		txtbxInspector.setColumns(10);

		txtbxDate = new JTextField();
		txtbxDate.setToolTipText("This will automatically be documented");
		txtbxDate.setEnabled(false);
		txtbxDate.setText("<calculated>");
		txtbxDate.setEditable(false);
		txtbxDate.setBounds(139, 194, 114, 19);
		frmToyCircuitData.getContentPane().add(txtbxDate);
		txtbxDate.setColumns(10);

		btnSave = new JButton("Save");
		btnSave.setToolTipText("Save the Entered Values");
		btnSave.addActionListener(new BtnSaveActionListener());
		btnSave.addKeyListener(new ClearValueKeyListener());
		btnSave.setBounds(20, 366, 154, 25);
		frmToyCircuitData.getContentPane().add(btnSave);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Circuit 1", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 225, 243, 135);
		frmToyCircuitData.getContentPane().add(panel);
		panel.setLayout(null);

		cboC1ManufactureLocation = new JComboBox<String>();
		cboC1ManufactureLocation.setToolTipText("Select the Manufacture Location for circuit 1");
		cboC1ManufactureLocation.addKeyListener(new ClearValueKeyListener());
		cboC1ManufactureLocation.setModel(new DefaultComboBoxModel<>(COUNTRIES));
		cboC1ManufactureLocation.setBounds(14, 100, 149, 24);
		panel.add(cboC1ManufactureLocation);

		txtC1Res = new JTextField();
		txtC1Res.setToolTipText("Enter the Resistance for circuit 1");
		txtC1Res.addActionListener(new BtnSaveActionListener());
		txtC1Res.addKeyListener(new ClearValueKeyListener());
		txtC1Res.addKeyListener(new LetterConsumeNoPeriodKeyListener());
		txtC1Res.setBounds(108, 49, 70, 19);
		panel.add(txtC1Res);
		txtC1Res.setColumns(10);

		JLabel lblC1Resistance = new JLabel("Resistance:");
		lblC1Resistance.setBounds(14, 54, 83, 14);
		panel.add(lblC1Resistance);

		JLabel lblC1Voltage = new JLabel("Voltage:");
		lblC1Voltage.setBounds(14, 29, 96, 14);
		panel.add(lblC1Voltage);

		txtC1Volt = new JTextField();
		txtC1Volt.setToolTipText("Enter the Voltage for circuit 1");
		txtC1Volt.addActionListener(new BtnSaveActionListener());
		txtC1Volt.addKeyListener(new ClearValueKeyListener());
		txtC1Volt.addKeyListener(new LetterConsumeNoPeriodKeyListener());
		txtC1Volt.setBounds(108, 26, 70, 19);
		panel.add(txtC1Volt);
		txtC1Volt.setColumns(10);

		JLabel lblManufactureLocation = new JLabel("Manufacture Location ");
		lblManufactureLocation.setBounds(14, 78, 217, 14);
		panel.add(lblManufactureLocation);

		JLabel lblVolts = new JLabel("Volts");
		lblVolts.setBounds(185, 29, 46, 14);
		panel.add(lblVolts);

		JLabel lblOhms = new JLabel("Ohms");
		lblOhms.setBounds(185, 52, 46, 14);
		panel.add(lblOhms);

		chckLock1 = new JCheckBox("Lock");
		chckLock1.addKeyListener(new ClearValueKeyListener());
		chckLock1.setBounds(165, 101, 70, 23);
		panel.add(chckLock1);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Circuit 2",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(285, 225, 243, 135);
		frmToyCircuitData.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		cboC2ManufactureLocation = new JComboBox<String>();
		cboC2ManufactureLocation.setToolTipText("Select the Manufacture Location for circuit 2");
		cboC2ManufactureLocation.addKeyListener(new ClearValueKeyListener());
		cboC2ManufactureLocation.setModel(new DefaultComboBoxModel<>(COUNTRIES));
		cboC2ManufactureLocation.setBounds(12, 100, 149, 24);
		panel_1.add(cboC2ManufactureLocation);

		JLabel lblC2Voltage = new JLabel("Voltage:");
		lblC2Voltage.setBounds(12, 27, 75, 19);
		panel_1.add(lblC2Voltage);

		txtC2Volt = new JTextField();
		txtC2Volt.setToolTipText("Enter the Voltage for circuit 2");
		txtC2Volt.addActionListener(new BtnSaveActionListener());
		txtC2Volt.addKeyListener(new ClearValueKeyListener());
		txtC2Volt.addKeyListener(new LetterConsumeNoPeriodKeyListener());
		txtC2Volt.setBounds(105, 27, 70, 19);
		panel_1.add(txtC2Volt);
		txtC2Volt.setColumns(10);

		JLabel lblC2Resistance = new JLabel("Resistance:");
		lblC2Resistance.setBounds(12, 52, 101, 14);
		panel_1.add(lblC2Resistance);

		txtC2Res = new JTextField();
		txtC2Res.setToolTipText("Enter the Resistance for circuit 2");
		txtC2Res.addActionListener(new BtnSaveActionListener());
		txtC2Res.addKeyListener(new ClearValueKeyListener());
		txtC2Res.addKeyListener(new LetterConsumeNoPeriodKeyListener());
		txtC2Res.setBounds(104, 52, 70, 19);
		panel_1.add(txtC2Res);
		txtC2Res.setColumns(10);

		JLabel label = new JLabel("Manufacture Location ");
		label.setBounds(12, 78, 219, 14);
		panel_1.add(label);

		JLabel label_1 = new JLabel("Volts");
		label_1.setBounds(185, 33, 46, 14);
		panel_1.add(label_1);

		JLabel label_2 = new JLabel("Ohms");
		label_2.setBounds(185, 55, 46, 14);
		panel_1.add(label_2);

		chckLock2 = new JCheckBox("Lock");
		chckLock2.addKeyListener(new ClearValueKeyListener());
		chckLock2.setBounds(166, 101, 65, 23);
		panel_1.add(chckLock2);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 130, 540, 14);
		frmToyCircuitData.getContentPane().add(separator);

		chckQuickEnter = new JCheckBox("Quick Enter");
		chckQuickEnter.setToolTipText("Check this to clear the form after every save.");
		chckQuickEnter.addKeyListener(new ClearValueKeyListener());
		chckQuickEnter.setBounds(209, 367, 114, 23);
		frmToyCircuitData.getContentPane().add(chckQuickEnter);

		frmToyCircuitData.getRootPane().setDefaultButton(btnSave);

		JLabel lblToyId = new JLabel("Toy ID:");
		lblToyId.setBounds(41, 170, 80, 14);
		frmToyCircuitData.getContentPane().add(lblToyId);

		txtToyID = new JTextField();
		txtToyID.setBounds(139, 172, 114, 20);
		frmToyCircuitData.getContentPane().add(txtToyID);
		txtToyID.addKeyListener(new ClearValueKeyListener());
		txtToyID.addKeyListener(new InvalidKeyStopperKeyListener());
		txtToyID.setColumns(10);

		JLabel lblNewLabel = new JLabel(
				"<html>\r\n<center>\r\nThis Form allows data entry on the Talking Salesman toy for quality control purposes.\r\n<br />\r\n<br />\r\n(All information is property of Buchan Berkstresser Enterprises. Only authorized employees may use this form.)\r\n</html>");
		lblNewLabel.setBounds(10, 0, 518, 107);
		frmToyCircuitData.getContentPane().add(lblNewLabel);

		JLabel lblPressEscTo = new JLabel("Press ESC to clear form");
		lblPressEscTo.setBounds(191, 96, 183, 22);
		frmToyCircuitData.getContentPane().add(lblPressEscTo);

		JButton btnLoadToy = new JButton("Load Toy");
		btnLoadToy.addActionListener(new BtnLoadToyActionListener());
		btnLoadToy.addKeyListener(new ClearValueKeyListener());
		btnLoadToy.setToolTipText("Save the Entered Values");
		btnLoadToy.setBounds(320, 187, 154, 25);
		frmToyCircuitData.getContentPane().add(btnLoadToy);

		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new BtnDeleteActionListener());
		btnDelete.setEnabled(false);
		btnDelete.setToolTipText("Delete Database Item");
		btnDelete.setBounds(374, 366, 154, 25);
		frmToyCircuitData.getContentPane().add(btnDelete);
		frmToyCircuitData.getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txtbxInspector, txtC1Volt, txtC1Res, txtC2Volt, txtC2Res, cboC1ManufactureLocation, cboC2ManufactureLocation, btnSave}));
		frmToyCircuitData.setFocusTraversalPolicy(
				new FocusTraversalOnArray(new Component[] { txtbxInspector, txtToyID, txtC1Volt, txtC1Res, txtC2Volt,
						txtC2Res, cboC1ManufactureLocation, cboC2ManufactureLocation, btnSave, btnClearValues }));
	}

	private class BtnSaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			//if any of the spaces are just a decimal, send an error
			if (txtC1Volt.getText().equals(".") || txtC2Volt.getText().equals(".") || txtC1Res.getText().equals(".")
					|| txtC2Res.getText().equals(".")) {
				JOptionPane.showMessageDialog(frmToyCircuitData, "Please do not leave one decimal in the space.");
				txtToyID.requestFocus();
				return;
			}

			//if any of the spaces are just a space, send an error
			if (txtC1Volt.getText().equals("") || txtC2Volt.getText().equals("") || txtC1Res.getText().equals("")
					|| txtC2Res.getText().equals("") || txtToyID.getText().equals("")) {
				JOptionPane.showMessageDialog(frmToyCircuitData, "Please do not leave an empty space.");
				txtToyID.requestFocus();
				return;
			}

			//if the inspector is empty, send an error
			if (txtbxInspector.getText().equals("")) {
				JOptionPane.showMessageDialog(frmToyCircuitData, "Please do not leave the Inpector Name blank.");
				txtToyID.requestFocus();
				return;
			}
			
			//if the manufacture location is empty (option 0), send an error
			if (cboC1ManufactureLocation.getSelectedItem().equals("") || cboC2ManufactureLocation.getSelectedItem().equals("") ) {
				JOptionPane.showMessageDialog(frmToyCircuitData, "Please choose a manufacture location.");
				txtToyID.requestFocus();
				return;
			}
			
			
			if (chckQuickEnter.isSelected()) {
				saveButton();
				clearForm();
			} else {
				saveButton();
				JOptionPane.showMessageDialog(frmToyCircuitData, "Item Saved");
			}

			

		}
	}

	private class ClearValueKeyListener extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			//if the escape key is pressed, clear all values
			if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
				clearForm();
			} // end if
		}// end keyAdapter
	}// end keylistener

	private class InvalidKeyStopperKeyListener extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			//if any key from 0 to 9 are pressed, consume them not allowing decimals
			if (e.getKeyChar() < KeyEvent.VK_0 || e.getKeyChar() > KeyEvent.VK_9) {
				e.consume();
				//if box already holds a decimal, consume any extra decimals
				if (e.getKeyChar() == KeyEvent.VK_PERIOD && ((JTextField) e.getSource()).getText().contains(".")) {
					e.consume();
				} // end if
			} // end if
		}// end keyAdapter
	}// end key listner

	private class LetterConsumeNoPeriodKeyListener extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			//if any key from 0 to 9 are pressed, consume them
			if (e.getKeyChar() < KeyEvent.VK_0 || e.getKeyChar() > KeyEvent.VK_9) {
				// if decimal is pressed, allow it
				if (e.getKeyChar() != KeyEvent.VK_PERIOD) {
					e.consume();
				}// end if
				//if box already holds a decimal, consume any extra decimals this is away from the letters
				if (e.getKeyChar() == KeyEvent.VK_PERIOD && ((JTextField) e.getSource()).getText().contains(".")) {
					e.consume();
				}//end if 
			}// end if
		}//end keytyped
	}//end keyadapter

	private class BtnLoadToyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			// if the toy id is empty, tell them
			if (txtToyID.getText().equals("")) {
				JOptionPane.showMessageDialog(frmToyCircuitData, "Please enter the toy ID.");
				txtToyID.requestFocus();
				return;
			}

			try {

				//load all values
				Toy myToy = new Toy();
				myToy.load(Integer.parseInt(txtToyID.getText()));

				btnDelete.setEnabled(true);

				txtToyID.setText(String.valueOf(myToy.getToyID()));
				txtbxInspector.setText(myToy.getInspector());
				txtbxDate.setText(String.valueOf(myToy.getInspectionDateTime()));
				txtbxDate.setEnabled(true);

				txtC1Res.setText(String.valueOf(myToy.getCircuit1().getResistance()));
				txtC1Volt.setText(String.valueOf(myToy.getCircuit1().getVoltage()));
				txtC2Res.setText(String.valueOf(myToy.getCircuit2().getResistance()));
				txtC2Volt.setText(String.valueOf(myToy.getCircuit2().getVoltage()));

				cboC1ManufactureLocation.setSelectedItem(myToy.getCircuit1().getManufactureLocation());
				cboC2ManufactureLocation.setSelectedItem(myToy.getCircuit2().getManufactureLocation());
			} catch (InvalidParameterException e) {
				
				JOptionPane.showMessageDialog(frmToyCircuitData, "Invalid Toy ID");
				txtToyID.requestFocus();
			}//end catch

		}//end actionpreformed
	}//end actionlistener

	private void saveButton() {

		//set the inspector
		myToy.setInspector(txtbxInspector.getText());

		// set the toyID
		myToy.setToyID(Integer.parseInt(txtToyID.getText()));

		myToy.setInspectionDateTime((new Date()));

		// set circuit 1 and 2 values
		myToy.getCircuit1().setVoltage(Double.parseDouble(txtC1Volt.getText()));
		myToy.getCircuit1().setResistance(Double.parseDouble(txtC1Res.getText()));
		myToy.getCircuit1().calculateAmperage();

		myToy.getCircuit2().setVoltage(Double.parseDouble(txtC2Volt.getText()));
		myToy.getCircuit2().setResistance(Double.parseDouble(txtC2Res.getText()));
		myToy.getCircuit2().calculateAmperage();

		myToy.getCircuit1().setManufactureLocation(cboC1ManufactureLocation.getSelectedItem().toString());
		myToy.getCircuit2().setManufactureLocation(cboC2ManufactureLocation.getSelectedItem().toString());

		myToy.getCircuit1().save();
		myToy.getCircuit2().save();

		// This is our goal

		myToy.save();

		// StringBuilder results = new StringBuilder();
		// results.append("\n----------------------");
		// results.append("\nToy Information");
		// results.append("\nInspector Name: " + myToy.getInspector());
		// results.append("\nInspection Date/Time: " +
		// myToy.getInspectionDateTime());
		// results.append("\nToyID: " + myToy.getToyID());
		// results.append("\n----------------------");
		// results.append("\nCircuit 1 Information");
		// results.append("\nCircuit ID: " +
		// myToy.getCircuit1().getCircuitID());
		// results.append("\nVoltage: " + myToy.getCircuit1().getVoltage());
		// results.append("\nAmperage: " +
		// myToy.getCircuit1().getAmperage());
		// results.append("\nResistance: " +
		// myToy.getCircuit1().getResistance());
		// results.append("\nLocation: " +
		// myToy.getCircuit1().getManufactureLocation());
		// results.append("\n----------------------");
		// results.append("\nCircuit 2 Information");
		// results.append("\nCircuit ID: " +
		// myToy.getCircuit2().getCircuitID());
		// results.append("\nVoltage: " + myToy.getCircuit2().getVoltage());
		// results.append("\nAmperage: " +
		// myToy.getCircuit2().getAmperage());
		// results.append("\nResistance: " +
		// myToy.getCircuit2().getResistance());
		// results.append("\nLocation: " +
		// myToy.getCircuit2().getManufactureLocation());
		// JOptionPane.showMessageDialog(null, results);
		// test
	}

	private void clearForm() {
		txtC1Res.setText("");
		txtC1Volt.setText("");
		txtC2Res.setText("");
		txtC2Volt.setText("");
		txtToyID.setText("");
		txtbxDate.setText("<calculated>");
		txtToyID.requestFocus();
		btnDelete.setEnabled(false);
		if (!chckLock1.isSelected()) {
			cboC1ManufactureLocation.setSelectedIndex(0);
		}
		if (!chckLock2.isSelected()) {
			cboC2ManufactureLocation.setSelectedIndex(0);
		}

	}

	private class BtnDeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			try {
				if (JOptionPane.showConfirmDialog(frmToyCircuitData, "Are you sure you want to delete the loaded item?",
						"Confirm Delete", JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
					myToy.setToyID(Integer.parseInt(txtToyID.getText()));
					myToy.delete();
					clearForm();
				} // end if
			} catch (InvalidParameterException e) {
				throw new InvalidParameterException("ToyID not found");
			} // end try catch

		}
	}

}
