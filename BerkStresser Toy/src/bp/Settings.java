package bp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Settings {
	
	public static String getServer() {
		try {
			Scanner settings = new Scanner(new File("settings.txt"));
			return settings.next();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
		
	}

}
