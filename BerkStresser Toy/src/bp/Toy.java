package bp;

import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import db.Database;
import db.Parameter;

/**
 * The Overall values of the Toy.
 * @author Michael
 *
 */
public class Toy implements IToy, IPermanentStorage {

	/**
	 * The ID of the toy.
	 */
	private int toyID;
	
	/**
	 * the inspectors name.
	 */
	private String inspector;
	
	/**
	 * time and date of inspection.
	 */
	private Date inspectionDateTime;
	
	/**
	 * creating the first circuit of the toy.
	 */
	private Circuit circuit1 = new Circuit(1);
	
	/**
	 * creating the second circuit of the toy.
	 */
	private Circuit circuit2 = new Circuit(2);

	/**
	 * return the toy ID.
	 */
	@Override
	public int getToyID() {
		return toyID;
	}

	/**
	 * return the toy inspector name.
	 */
	@Override
	public String getInspector() {
		return inspector;
	}
	
	/**
	 * return the inspection date and time.
	 * this is automatically generated
	 * no input from user
	 */
	@Override
	public Date getInspectionDateTime() {
		return inspectionDateTime;
	}

	/**
	 * return the first circuit.
	 */
	@Override
	public Circuit getCircuit1() {
		return circuit1;
	}

	/**
	 * return the second circuit.
	 */
	@Override
	public Circuit getCircuit2() {
		return circuit2;
	}

	/**
	 * set the toy ID.
	 */
	@Override
	public void setToyID(final int pToyID) {
		toyID = pToyID;
		circuit1.setToyID(pToyID);
		circuit2.setToyID(pToyID);
	}

	/**
	 * set the inspector.
	 */
	@Override
	public void setInspector(final String pInspector) {
		inspector = pInspector;
	}

	/**
	 * set the inspection date and time.
	 * this is automatic and requires no 
	 * input from the user
	 */
	@Override
	public void setInspectionDateTime(final Date pInspectionDateTime) {
		inspectionDateTime = pInspectionDateTime;
	}

	/**
	 * set the first circuit values.
	 */
	@Override
	public void setCircuit1(final Circuit pCircuit1) {
		circuit1 = pCircuit1;
	}

	/**
	 * set the second circuit values.
	 */
	@Override
	public void setCircuit2(final Circuit pCircuit2) {
		circuit2 = pCircuit2;
	}

	/**
	 * save all the values to the database.
	 */
	@Override
	public void save() {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		// set the parameter values from the properties
		params.add(new Parameter<Integer>(toyID));
		params.add(new Parameter<String>(inspector));
		params.add(new Parameter<Date>(inspectionDateTime));

		// save
		db.executeSql("usp_SaveToy", params);
		//save both circuits
		circuit1.save();
		circuit2.save();

	}

	/**
	 * Delete the loaded value from the database.
	 */
	@Override
	public void delete() {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();
		params.add(new Parameter<Integer>(toyID));
		db.executeSql("usp_DeleteToy", params);
		
		
		
	}

	/**
	 * load the toy information from the database.
	 * based on the given ID of the toy
	 */
	@Override
	public void load(final int... id) {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter<Integer>(id[0]));

		ResultSet toys = db.getResultSet("usp_GetToy", params);

		//try loading, if it does not work break easily
		try {
			if (toys.next()) {
				toyID = toys.getInt("ToyID");
				inspectionDateTime = toys.getDate("InspectionDateTime");
				inspector = toys.getString("Inspector");
				circuit1.load(toyID, 1);
				circuit2.load(toyID, 2);

			} else {
				throw new InvalidParameterException("ToyID not found");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
}
