package bp;

import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.Database;
import db.Parameter;

/**
 * The circuit class holding the values of the circuits.
 * @author Michael
 *
 */
public class Circuit implements ICircuit, IPermanentStorage {
	
	/**
	 * The toy ID.
	 */
	private int toyID;
	
	/**
	 * the circuit ID (1 or 2).
	 */
	private int circuitID;
	
	/**
	 * the amperage of the circuit.
	 */
	private double amperage;
	
	/**
	 * the voltage of the circuit.
	 */
	private double voltage;
	
	/**
	 * the resistance of the circuit.
	 */
	private double resistance;
	
	/**
	 * the manufacture location.
	 */
	private String manufactureLocation;

	/**
	 * this creates the circuit ID and sets it.
	 * @param pCircuitID
	 * circuitID
	 */
	public Circuit(final int pCircuitID) {
		circuitID = pCircuitID;
	}
	
	/**
	 * return the toyID.
	 */
	@Override
	public int getToyID() {
		return toyID;
	}

	
	/**
	 * return the circuit ID.
	 */
	@Override
	public int getCircuitID() {
		return circuitID;
	}

	
	/**
	 * return the voltage.
	 */
	@Override
	public double getVoltage() {
		return voltage;
	}

	/**
	 * return the amperage.
	 */
	@Override
	public double getAmperage() {
		return amperage;
	}
	
	/**
	 * return the resistance.
	 */
	@Override
	public double getResistance() {
		return resistance;
	}

	/**
	 * return the manufacture location.
	 */
	@Override
	public String getManufactureLocation() {
		return manufactureLocation;
	}

	
	/**
	 * set the ToyID.
	 */
	@Override
	public void setToyID(final int ptoyID) {
		toyID = ptoyID;
	}

	/**
	 * set the circuitID (for some reason I set this twice).
	 */
	@Override
	public void setCircuitID(final int pCircuitID) {
		circuitID = pCircuitID;
	}

	/**
	 * set the voltage.
	 */
	@Override
	public void setVoltage(final double pVoltage) {
		voltage = pVoltage;
	}
	
	/**
	 * set the amperage.
	 */
	@Override
	public void setAmperage(final double pAmperage) {
		amperage = pAmperage;
	}

	/**
	 * set the resistance.
	 */
	@Override
	public void setResistance(final double pResistance) {
		resistance = pResistance;
	}

	/**
	 * set the manufacture location.
	 */
	@Override
	public void setManufactureLocation(final String pManufactureLocation) {
		manufactureLocation = pManufactureLocation;
	}

	/**
	 * calculates the voltage of the circuit.
	 */
	@Override
	public void calculateVoltage() {
		voltage = amperage * resistance;
	}

	/**
	 * calculated the amperage of the circuit.
	 */
	@Override
	public void calculateAmperage() {
		amperage = voltage / resistance;
	}

	/**
	 * calculates the resistance of the circuit.
	 */
	@Override
	public void calculateResistance() {
		resistance = voltage / amperage;
	}

	/**
	 * tells if the calculation is valid or not.
	 * I dont really think this is necessary
	 * but it seems to be needed
	 */
	@Override
	public boolean isValid() {
		return voltage == amperage * resistance;
	}

	/**
	 * save the circuit information.
	 */
	@Override
	public void save() {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		// set the parameter values from the properties
		params.add(new Parameter<Integer>(toyID));
		params.add(new Parameter<Integer>(circuitID));
		params.add(new Parameter<String>(manufactureLocation));
		params.add(new Parameter<Double>(voltage));
		params.add(new Parameter<Double>(amperage));
		params.add(new Parameter<Double>(resistance));

		// save
		db.executeSql("usp_SaveCircuit", params);

	}
	
	/**
	 * clear the data from the local memory.
	 * Not used
	 */
	@Override
	public void clear() {

	}

	/**
	 * delete the circuit from the database.
	 * Not used
	 */
	@Override
	public void delete() {

	}

	/**
	 * load data from the database.
	 */
	@Override
	public void load(final int... id) {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter<Integer>(id[0]));

		params.add(new Parameter<Integer>(id[1]));
		
		ResultSet circuits = db.getResultSet("usp_GetCircuit", params);

		try {
			if (circuits.next()) {
				toyID = circuits.getInt("ToyID");
				 amperage = circuits.getDouble("amperage");
				 voltage = circuits.getDouble("voltage");
				 resistance = circuits.getDouble("resistance");
				 manufactureLocation = circuits.getString("manufactureLocation");

			} else {
				throw new InvalidParameterException("ToyID not found");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
}
